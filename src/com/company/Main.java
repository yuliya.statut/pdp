package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Shape> figures = new ArrayList<Shape>();

        Shape triangle = new Triangle(3, 4, 5);
        Shape square = new Square(5);
        Shape rectangle = new Rectangle(3, 4);

        figures.add(triangle);
        figures.add(square);
        figures.add(rectangle);

        for (Shape e : figures) {
            System.out.println(e);
        }
    }
}
