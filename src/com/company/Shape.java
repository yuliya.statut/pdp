package com.company;

/**
 * <p>
 * <p> Copyright &copy; 2018 Edmunds.com
 * Date: 10/4/2018
 *
 * @author Ihar_Krupenin
 */
public abstract class Shape {

    public abstract double getSquare();
    public abstract double getPerimeter();

    @Override
    public String toString() {
        return String.format("%s with perimeter: %.2f, and square: %.2f", getClass().getSimpleName(), getPerimeter(), getSquare());
    }
}
